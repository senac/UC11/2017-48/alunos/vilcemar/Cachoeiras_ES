package br.com.senac.cachoeirases;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btVeudaniva = findViewById(R.id.parq_ca_veu_noiva);
        Button btRiomeio = findViewById(R.id.ca_rio_meio);
        Button btCapalito = findViewById(R.id.ca_palito);
        Button btMatilde = findViewById(R.id.ca_matilde);

        btVeudaniva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,Detalhes_Activity.class);
                startActivity(intent);
            }
        });

        btRiomeio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,RioMeioActivity.class);
                startActivity(intent);
            }
        });

        btCapalito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,PalitoActivity.class);
                startActivity(intent);
            }
        });

        btMatilde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,CachMatActivity.class);
                startActivity(intent);
            }
        });







    }
}
